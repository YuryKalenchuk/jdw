package modulo;

import java.util.Arrays;
import java.util.Scanner;

public class ArraysSort {
    private static int[] changeNumsByIndex(int arr[],int i,int j){

return arr;
    }
    private static int getRandomIntBetweenRange(int min, int max) {
        int num = (int) (Math.random() * ((max - min) + 1)) + min;
        return num;
    }

    private static int[] createRandomIntegerArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of elements n: ");
        int n = in.nextInt();
        System.out.println("Array has elements from A to B: ");
        System.out.println("Input A: ");
        int start = in.nextInt();
        System.out.println("Input B: ");
        int finish = in.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = getRandomIntBetweenRange(start, finish);
        }
        printArray(array);
        return array;
    }

    public static void printArray(int[] array) {
        System.out.println("\n");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void task1(int arr1[], int arr2[], int m) {
        System.out.println("1. Заданы два одномерных массива с различным количеством элементов и натуральное число k. Объединить их в\n" +
                "    один массив, включив второй массив между k-м и (k+1) - м элементами первого, при этом не используя\n" +
                "    дополнительный массив.");
        int k = 0;
        int[] newarr = new int[arr1.length + arr2.length];
        for (int i = 0; i < arr1.length; i++) {
            newarr[i] = arr1[i];
            if (i == m) {
                for (int j = ++m; k < arr2.length; j++) {
                    newarr[j] = arr2[k];
                    k++;
                }
                i += arr2.length;
            }
        }

        for (int i = m; i < arr1.length; i++) {
            newarr[m + arr2.length] = arr1[i];
            m++;
        }
        for (int i : newarr
        ) {
            System.out.println(i);
        }
    }


    public static void task3(int[] arr) {
        System.out.println("3. Сортировка выбором. Дана последовательность чисел\n" +
                "a1>= a2 >= an\n" +
                ".Требуется переставить элементы так,\n" +
                "чтобы они были расположены по убыванию. Для этого в массиве, начиная с первого, выбирается наибольший элемент\n" +
                "и ставится на первое место, а первый - на место наибольшего. Затем, начиная со второго, эта процедура повторяется.\n" +
                "Написать алгоритм сортировки выбором.");
        for (int i = 0; i <= arr.length / 2; i++) {
            int start = arr[i];
            int end = arr[arr.length - i - 1];
            int buffer = arr[i];
            if (start <= arr[i + 1] && end >= arr[arr.length - i - 1]) {
                arr[i] = end;
                arr[arr.length - i - 1] = buffer;
            }
        }
        printArray(arr);
    }

    static void task4(int[] arr) {
        System.out.println("Дана последовательность чисел\n" +
                "a1 a2 an\n" +
                ".Требуется переставить числа в порядке\n" +
                "возрастания. Для этого сравниваются два соседних числа\n" +
                "ai и ai+1\n" +
                ". Если\n" +
                "ai > ai+1\n" +
                ", то делается перестановка. Так\n" +
                "продолжается до тех пор, пока все элементы не станут расположены в порядке возрастания. Составить алгоритм\n" +
                "сортировки, подсчитывая при этом количества перестановок.");
        int count = 0;
        while (true) {
            int localCount = count;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    count++;
                }
            }
            if (count == localCount) break;
        }
        System.out.println("Count of changes is "+ count);
    }
public static void task5(int[]arr){

}

    public static void main(String[] args) {
        int[] arr = createRandomIntegerArray();

        task4(arr);
        printArray(arr);
//        int[] arr1 = {1, 2, 3, 4, 7};
//        int[] arr2 = {9, 10, 23, 56};
//        task1(arr1, arr2, 1);
    }
}
