package modulo;


import java.math.BigInteger;

public class Cycles {
    public static void task1() {
        System.out.println("Task1: ");
        for (int i = 1; i < 6; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void task2() {
        System.out.println("Task2: ");
        for (int i = 5; i > 0; i--) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void task3() {
        System.out.println("Task3: ");
        for (int i = 1; i < 11; i++) {
            System.out.println("3 x " + i + " = " + i * 3);
        }
    }

    public static void task4() {
        System.out.println("Task4: ");
        int i = 2;
        while (i < 101) {
            System.out.print(i + " ");
            i += 2;
        }
        System.out.println();
    }

    public static void task5() {
        System.out.println("Task5: ");
        int i = 1;
        int sum = 0;
        while (i < 100) {
            sum += i;
            i += 2;
        }
        System.out.println("Summ = " + sum);
    }

    public static void task6(int a) {
        System.out.println("Task6: ");
        System.out.println("S= " + (a + a * a) / 2);
    }


    public static void task7(int a, int b, int h) {
        for (int i = a; i < b; i += h) {
            if (i > 2) {
                System.out.print("[x= " + i + " y= " + i + " ]");
            } else {
                System.out.print("[x= " + i + " y= " + (-i) + " ]");
            }
        }
        System.out.println();
    }

    public static void task8(int a, int b, int h) {
        for (int i = a; i < b; i += h) {
            if (i == 15) {
                System.out.print("[x= " + i + " y= " + 30 + " +2C]");
            } else {
                System.out.print("[x= " + i + " y= " + (i + 6) + " -C]");
            }
        }
    }

    public static void task9() {
        long sum = 0;
        for (long i = 0; i < 100; i++) {
            sum = i * i + sum;
        }
        System.out.println("Sum = " + sum);
    }

    public static void task10() {
        BigInteger multi = new BigInteger(String.valueOf(1));

        for (int i = 1; i < 200; i++) {
            BigInteger small = new BigInteger(String.valueOf(i));
            multi = multi.multiply(small);
        }
        multi=multi.pow(2);
        System.out.println("Multiple of squads of 1-200 = " + multi.toString());
    }

    public static void main(String[] args) {
//        task1();
//        task2();
//        task3();
//        task4();
//        task5();
//        task6(11);
//        task7(12,18,1);
//        task8(12,18,1);
//        task9();
   task10();
    }
}
