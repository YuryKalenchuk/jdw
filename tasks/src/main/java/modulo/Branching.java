package modulo;

public class Branching {
    public static void task1(int a, int b) {
        if (a < b) {
            System.out.println("7");
        } else {
            System.out.println("8");
        }
    }

    public static void task2(int a, int b) {
        if (a < b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    public static void task3(int a) {
        if (a < 3) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    public static void task4(int a, int b) {
        if (a == b) {
            System.out.println("Numbers are Equials");
        } else {
            System.out.println("Not equials");
        }
    }

    public static void task5(int a, int b) {
        if (a < b) {
            System.out.println("1st num is smalest");
        } else {
            if (a == b) {
                System.out.println("Equials");
            } else {
                System.out.println("2nd num is smalest");
            }
        }
    }

    public static void task6(int a, int b) {
        if (a < b) {
            System.out.println("2nd num is biggest");
        } else {
            if (a == b) {
                System.out.println("Equials");
            } else {
                System.out.println("1nd num is biggest");
            }
        }
    }

    public static void task7(int a, int b, int c, int x) {
        System.out.println("module = " + Math.abs(a * x * x + (b * x)));
    }

    public static void task8(double a, double b) {
        if (a * a > b * b) {
            System.out.println("квадрат B наименьший");
        } else {
            System.out.println("Квадрат А наименьший");
        }
        if (a == b) {
            System.out.println("Числа равны");
        }
    }

    public static void task9(int a, int b, int c) {
        if (a == b && b == c) {
            System.out.println("Restringle has similar sides");
        } else {
            System.out.println("Sides are not similar");
        }
    }

    public static void task10(int a, int b) {
        if (a > b && b >= 0) {
            System.out.println("S of 1st round is bigger");
        }
        if (b > a && a >= 0) {
            System.out.println("S of 2st round is bigger");
        }
        if (a < 0 && b < 0) {
            System.out.println("Are u kidding me?");
        }
    }

    public static void task11(double a, double b, double c, double a1, double b1, double c1) {
        if (a <= 0 || b <= 0 || c <= 0 || a1 <= 0 || b1 <= 0 || c1 <= 0) {
            System.out.println("Are u kidding me?");
        } else {
            double p = (a + b + c) / 2;
            double p1 = (a1 + b1 + c1) / 2;
            double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
            double s1 = Math.sqrt(p1 * (p1 - a1) * (p1 - b1) * (p1 - c1));
            if (s > s1) {
                System.out.println("S of 1 st Tringle is Bigger ");
            } else {
                if (s == s1) {
                    System.out.println("S of tringles is similar");
                } else {
                    System.out.println("S of 2nd Tringle is Bigger ");
                }
            }

        }
    }
    public static void task12(double a,double b, double c){
        if(a>0){
            System.out.println("A = "+a*a);
        }
        else{
            System.out.println("A = "+a*a*a*a);
        }
        if(b>0){
            System.out.print("B = "+b*b+" ");
        }
        else{
            System.out.print("B = "+b*b*b*b + " ");
        }
        if(c>0){
            System.out.print("C = "+c*c);
        }
        else{
            System.out.print("C = "+c*c*c*c);
        }
        System.out.println();
    }
    public static void task40(int x){
        if (x>13){
            float f;
            f = -3/(x+1);
            System.out.println(f);
        }
        else{
            System.out.println("F(x) = "+(9-(x*x*x)));
        }
    }

    public static void main(String[] args) {
//        task1(2, 3);
//        task2(3, 2);
//        task3(2);
//        task4(2, 2);
//        task5(3, 4);
//        task6(2, 1);
//        task7(1, -2, 4, -1);
//        task8(1, 0.9);
//        task9(1,2,6);
//        task10(1, 5);
//        task11(1,2,3,1,2,4);
//        task12(-3,3,1.1);
//        task40(14);
    }
}
