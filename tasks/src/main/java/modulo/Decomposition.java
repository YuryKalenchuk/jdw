package modulo;

import java.util.Scanner;

public class Decomposition {
    static boolean isGrowing(int num) {
        String str = Integer.toString(num);
        char[] arr = str.toCharArray();
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] >= arr[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static boolean isConsistOnlyOddNums(int num) {
        String str = Integer.toString(num);
        char[] arr = str.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            int checked = Character.digit(arr[i], 10);
            if (checked % 2 == 0) {
                return false;
            }
        }
        return true;
    }

    static int countOfEvenNums(int num) {
        String str = Integer.toString(num);
        char[] arr = str.toCharArray();
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            int checked = Character.digit(arr[i], 10);
            if (checked % 2 == 0) {
                counter++;
            }
        }
        return counter;
    }

    static boolean armstrongNumCheck(int num) {
        String str = Integer.toString(num);
        int multiple = str.length();
        int sum = 0;
        char[] arr = str.toCharArray();
        for (char i : arr) {
            sum += Math.pow(Character.digit(i, 10), multiple);
        }
        return sum == num;
    }

    static void printPairs(int num) {
        System.out.println("Pair is " + num + " and " + (num + 2));
    }

    static int calculateFactorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    static void printArray(int[] array) {
        System.out.println("\n");
        for (int value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    static int getRandomIntBetweenRange(int min, int max) {
        int num = (int) (Math.random() * ((max - min) + 1)) + min;
        return num;
    }

    public static int[] createRandomPositiveArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of elements N: ");
        int n = in.nextInt();
        System.out.println("Array has elements from 0 to M: ");
        System.out.println("Input M: ");
        int m = in.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomIntBetweenRange(0, m);
        }
        printArray(array);
        return array;
    }

    static int getSumOf3Num(int[] arr, int k, int m) {
        int sum = 0;
        for (int i = k - 1; i < m && i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    static double sqOfTringle(int a) {
        return Math.sqrt(3) * a * a / 4;
    }

    static int maxOfThree(int a, int b, int c) {
        int buffer = Math.max(a, b);
        if (buffer > c) {
            return buffer;
        } else return c;
    }

    static int minOfThree(int a, int b, int c) {
        int buffer;
        if (a < b) {
            buffer = a;
        } else buffer = b;
        return Math.min(buffer, c);
    }

    public static Double mathLenthOnKoord(double x1, double y1, double x2, double y2) {
        Double d = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        return d;
    }

    public static int nod(int a, int b) {
        while (a != 0 && b != 0) {
            if (a > b) {
                a = a % b;
            } else {
                b = b % a;
            }
        }
        return a + b;
    }

    static int sumOfdigit(int num) {
        String str = Integer.toString(num);
        int sum = 0;
        char[] arr = str.toCharArray();
        for (char i : arr) {
            sum += Character.digit(i, 10);
        }
        return sum;
    }

    public static void task1(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = mathLenthOnKoord(x1, y1, x2, y2);
        double b = mathLenthOnKoord(x2, y2, x3, y3);
        double c = mathLenthOnKoord(x1, y1, x3, y3);
        double p = (a + b + c) / 2;
        double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        System.out.println("Task 13: P= " + (a + b + c) + " S= " + s);
    }


    public static int task2(int a, int b) {
        System.out.println("NOK of " + a + " and " + b + " = " + a * b / nod(a, b));
        return a * b / nod(a, b);
    }

    public static void task3(int a, int b, int c, int d) {
        System.out.println("NOD of 4 num = " + nod(nod(a, b), nod(c, d)));
    }

    public static void task4(int a, int b, int c) {
        int nok = task2(task2(a, b), c);
        System.out.println("NOK of " + a + " & " + b + " & " + c + " = " + nok);
    }

    public static void task5(int a, int b, int c) {
        System.out.println("Sum of max and min nums = " + (maxOfThree(a, b, c) + minOfThree(a, b, c)));
    }

    static void task6(int a) {
        System.out.println(" S = " + 6 * sqOfTringle(a));
    }

    static void task8() {
        System.out.println("8. Составить программу, которая в массиве A[N] находит второе по величине число (вывести на печать число,\n" +
                "которое меньше максимального элемента массива, но больше всех других элементов).\n");
        int[] arr = createRandomPositiveArray();
        int max = arr[0];
        int buf = 0;
        for (int i : arr) {
            if (i > max) {
                buf = max;
                max = i;
            }
        }
        System.out.println("Второе по величине число = " + buf);
    }

    static void task9(int a, int b, int c) {
        if (nod(nod(a, b), c) == 1) {
            System.out.println("Числа взаимопростые");
        } else System.out.println("Числа не взаимопростые");
    }

    static void task10() {
        int i = 1;
        int sum = 0;
        while (i < 10) {
            if (i % 2 == 1) {
                sum += calculateFactorial(i);
                i++;
            } else {
                i++;
            }
        }
        System.out.println("Sum of ODD factorials = " + sum);

    }

    static void task11() {
        int[] arr = createRandomPositiveArray();
        System.out.println("Sum of D[l] + D[2] + D[3] = " + getSumOf3Num(arr, 1, 3));
        System.out.println("Sum of D[3] + D[4] + D[5] = " + getSumOf3Num(arr, 3, 5));
        System.out.println("Sum of D[4] + D[5] + D[6] = " + getSumOf3Num(arr, 4, 6));
    }

    static void task12(double x, double y, double z, double t) {
        System.out.println("12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления его площади,\n" +
                "если угол между сторонами длиной X и Y— прямой.");
        double s = 0;
//        double x=3;
//        double y=4;
//        double z=7;
//        double t=5;
        s += x * y / 2;
        double a = Math.sqrt(x * x + y * y);
        double p = (a + z + t) / 2;
        double s1 = Math.sqrt(p * (p - a) * (p - z) * (p - t));
        s += s1;
        System.out.println("Площадь четырехугольника равна " + s);
    }

    static void task13(int num) {
        String str = Integer.toString(num);
        char[] arr = str.toCharArray();
        System.out.println("Start arr :");
        for (char i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    static void task14(int a, int b) {
        String strA = Integer.toString(a);
        String strB = Integer.toString(b);
        if (strA.length() > strB.length()) {
            System.out.println("1st num has more digits");
        } else if (strA.length() < strB.length()) {
            System.out.println("2nd num has more digits");
        } else {
            System.out.println("Nums has same digits");
        }
    }

    static void task15(int k, int n) {
        int counter = 0;
        for (int i = n; i > 0; i--) {
            if (sumOfdigit(i) == k) {
                counter++;
            }
        }
        if (counter == 0) {
            System.out.println("Wrong Input");
        }
        int[] arr = new int[counter];
        for (int i = n; i > 0; i--) {
            if (sumOfdigit(i) == k) {
                arr[counter - 1] = i;
                counter--;
            }
        }
        printArray(arr);
    }

    static void task16() {
        System.out.println("Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43). Найти\n" +
                "и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2. Для решения\n" +
                "задачи использовать декомпозицию.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter N plz");
        int n = sc.nextInt();
        for (int i = n; i <= 2 * n - 2; i++) {
            printPairs(i);
        }
    }

    static void task17() {
        System.out.println(" Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр, возведенная\n" +
                "в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи использовать\n" +
                "декомпозицию.\n");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter K plz");
        int k = sc.nextInt();
        System.out.println("Armstrong nums from 0 to " + k + " :");
        for (int i = k; i > 0; i--) {
            if (armstrongNumCheck(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    static void task18() {
        System.out.println(" Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность\n" +
                "(например, 1234, 5789). Для решения задачи использовать декомпозицию.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter N plz");
        int n = sc.nextInt();
        int num = (int) Math.pow(10, n) - 1;
        System.out.println("Growing nums is ");
        for (int i = num; i > (int) Math.pow(10, n - 1); i--) {
            if (isGrowing(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    static void task19() {
        System.out.println("Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. Определить\n" +
                "также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter N plz");
        int n = sc.nextInt();
        int sum = 0;
        int num = (int) Math.pow(10, n) - 1;
        for (int i = num; i > (int) Math.pow(10, n - 1); i--) {
            if (isConsistOnlyOddNums(i)) {
                sum += i;
            }
        }
        System.out.println("Summ = " + sum);
        System.out.println("Count of Even digits = " + countOfEvenNums(sum));
    }

    static void task20() {
        System.out.println("Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. Сколько таких\n" +
                "действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter N plz");
        int n = sc.nextInt();
        int counter = 0;
        while (n != 0) {
            n -= sumOfdigit(n);
            counter++;
        }
        System.out.println("Count of actions = " + counter);
    }

    public static void main(String[] args) {
        task20();

    }
}
