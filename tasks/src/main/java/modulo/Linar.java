package modulo;

public class Linar {
    public static void task1(double x, double y) {
        System.out.println("summ = " + (x + y));
        System.out.println("subtraction= " + (x - y));
        System.out.println("multiple= " + (x * y));
        System.out.println("division= " + (x / y));
    }

    public static void task2(int a) {
        System.out.println("c= " + (a + 3));
    }

    public static void task3(double x, double y) {

        System.out.println("z = " + ((2 * x) + 5 * (y - 2)));
    }

    public static void task4(double a, double b, double c) {
        System.out.println("z = " + (((a - 3) * b) / 2) + c);
    }

    public static void task5(double a, double b) {
        System.out.println("average is " + (a + b) / 2);
    }

    public static void task6(int n) {
        System.out.println("m = " + (12 * n + 80));
    }

    public static void task7(int n) {
        System.out.println("S=" + n * n / 2 + "P= " + 3 * n + " если введена длинна");
        System.out.println("S=" + n * n * 2 + "P= " + 5 * n + " если введена ширина");
    }

    public static void task8(int a, int b, int c) {
        System.out.println(((b + Math.sqrt((b * b) + (4 * a * c))) / 2 / a) - (a * a * a * c) + Math.sqrt(2));
    }

    public static void task9(double a, double b, double c, double d) {
        System.out.println("Task9: " + (((a * b / c) / d) - ((a * b - c) / (c * d))));
    }

    public static void task10(double x, double y) {
        System.out.println("Task 10: " + (Math.sin(Math.toRadians(x)) + Math.cos(Math.toRadians(y))) / (Math.cos(Math.toRadians(x)) - Math.sin(Math.toRadians(y))) * Math.tan(Math.toRadians(x * y)));
    }

    public static void task11(double a, double b) {
        System.out.println("Task11: S= " + a * b / 2 + "  P= " + (a + b + Math.sqrt(a * a + b * b)));
    }

    public static void task12(double x1, double y1, double x2, double y2) {
        Double d = mathLenthOnKoord(x1, y1, x2, y2);
        System.out.println("Task 12: L= " + d);
    }

    public static Double mathLenthOnKoord(double x1, double y1, double x2, double y2) {
        Double d = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        return d;
    }

    public static void task13(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = mathLenthOnKoord(x1, y1, x2, y2);
        double b = mathLenthOnKoord(x2, y2, x3, y3);
        double c = mathLenthOnKoord(x1, y1, x3, y3);
        double p = (a + b + c) / 2;
        double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        System.out.println("Task 13: P= " + (a + b + c) + " S= " + s);
    }

    public static void task14(double r) {
        System.out.println("Task14: L= " + 2 * Math.PI * r + " S= " + Math.PI * r * r);
    }

    public static void task15() {
        System.out.println("Task 15:  PI^0 = 1  PI^1= " + Math.PI + " PI^2 = " + Math.PI * Math.PI + " PI^3 = " + Math.PI * Math.PI * Math.PI);
    }

    public static Integer sumOfNumbers(int num) {
        int sum = 0;
        while (Math.abs(num) >= 1) {
            if (Math.abs(num) > 10) {
                sum += Math.abs(num % 10);
                num /= 10;
            } else {
                sum += Math.abs(num);
                break;
            }
        }
        return Math.abs(sum);
    }

    public static void task16(int number) {
        if (Math.abs(number) > 999 && Math.abs(number) < 9999) {
            System.out.println("Task16: Sum = " + sumOfNumbers(number));
        } else {
            System.out.println("Are u kidding me?");
        }
    }

    public static void task17(double a, double b) {
        System.out.println("Task17: S= " + (a * a * a + b * b * b) / 2 + " SM = " + Math.sqrt(a * b));
    }

    public static void task18(double a) {
        System.out.println("Task18: S gr = " + a * a + " S av = " + 6 * a * a + " V = " + a * a * a);
    }

    public static void task19(double a) {
        System.out.println("Task 19: S tr = " + a / 4 * a * Math.sqrt(3) + "; h tr = " + a * Math.sqrt(3) / 2 + "; R in =" + a / (2 * Math.sqrt(3)) + "; R out = " + a / Math.sqrt(3));
    }

    public static void task20(double a) {
        System.out.println("Task20: S= " + a * a / 4 / Math.PI);
    }

    public static void task21(double r) {
        int full = (int) r;
        double parts = r % 1 * 1000;
        System.out.println("Task21: num = " + (Math.round(parts) + (double) full / 1000));
    }

    public static void task22(int t) {
        int h = 0;
        int m = 0;
        int s = 0;
        if (t / 3600 > 1) {
            h = (int) Math.floor(t / 3600);
        }
        int min = t - (h * 3600);
        if (min > 60) {
            m = (t - (h * 3600)) / 60;
        }
        int sec = t - (h * 3600) - (m * 60);
        if (sec > 0) {
            s = sec;
        }
        System.out.println("Task22: " + h + "h " + m + "min " + s + "sec");
    }

    public static void task23(int rIn, int rOut) {
        double sIn = Math.PI * rIn * rIn;
        double s = Math.PI * rOut * rOut - sIn;
        System.out.println("Task23: S ring = " + s);
    }

    public static void task24(int a, int b, int con) {
        //a>b
        double t = Math.round(Math.tan(Math.toRadians(con)));
        System.out.println("Task24: S= " + t / 4 * ((a * a) - (b * b)));
    }

    public static void task25(int a, int b, int c) {
        double d = b * b - 4 * a * c;
        double x1 = (-b + Math.sqrt(d)) / 2 / a;
        double x2 = (-b - Math.sqrt(d)) / 2 / a;
        System.out.println("Task25: X1 = " + x1 + " X2 = " + x2);
    }

    public static void main(String[] args) {
//        task1(1, 2);
//        task2(3);
//        task3(2, 4);
//        task4(3, 2, 1);
//        task5(2, 4);
//        task6(10);
//        task7(6);
//        task8(1, 2, 3);
//        task9(1, 2, 3, 4);
//        task10(45, 45);
//        task11(3,4);
//        task12(1, 1, 5, 4);
//        task13(1, 1, 2, 1, 2, 2);
//        task14(1);
//        task15();
//        task16(-3250);
//        task17(8,2);
//        task18(2);
//        task19(2);
//        task20(3);
//        task21(221.341);
//        task22(123900);
//        task23(2,3);
//        task24(4, 2, 45);
//      task25(1, 4, 3);

    }
}