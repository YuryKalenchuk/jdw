package modulo;

import java.util.Scanner;

public class MultiArr {
    static void printMatrix(int[][] array) {
        for (int[] ints : array) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");
            }
            System.out.println();
        }
    }

    static void printArray(int[] array) {
        System.out.println("\n");
        for (int i1 : array) {
            System.out.print(i1 + " ");
        }
        System.out.println();
    }

    private static int[] createRandomIntegerArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of elements n: ");
        int n = in.nextInt();
        System.out.println("Array has elements from A to B: ");
        System.out.println("Input A: ");
        int start = in.nextInt();
        System.out.println("Input B: ");
        int finish = in.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = getRandomIntBetweenRange(start, finish);
        }
        printArray(array);
        return array;
    }

    private static int getRandomIntBetweenRange(int min, int max) {
        int num = (int) (Math.random() * ((max - min) + 1)) + min;
        return num;
    }

    static int[][] createMatrix() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of length elements n: ");
        int n = in.nextInt();
        System.out.print("Input amount of higher elements m: ");
        int m = in.nextInt();
        System.out.println("Matrix has elements from A to B: ");
        System.out.println("Input A: ");
        int start = in.nextInt();
        System.out.println("Input B: ");
        int finish = in.nextInt();
        int[][] matrix = new int[m][n];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = getRandomIntBetweenRange(start, finish);
            }
        }
        printMatrix(matrix);
        return matrix;
    }

    static void task1() {
        int[][] matrix = new int[3][4];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][getRandomIntBetweenRange(0, matrix[i].length - 1)] = 1;
        }
        printMatrix(matrix);
    }

    static void task2() {
        System.out.println("2. Создать и вывести на экран матрицу 2 x 3, заполненную случайными числами из [0, 9].");
        int[][] matrix = createMatrix();
    }

    static void task3() {
        System.out.println("3. Дана матрица. Вывести на экран первый и последний столбцы.");
        int[][] matrix = createMatrix();
        for (int j = 0; j < matrix[0].length; j++) {
            if (j == 0 || j == matrix[0].length - 1) {
                System.out.print("Столбец №" + (j + 1) + " : ");
                for (int[] matrix1 : matrix) {
                    System.out.print(matrix1[j] + " ");
                }
                System.out.println();
            }
        }
    }

    static void task4() {
        System.out.println("4. Дана матрица. Вывести на экран первую и последнюю строки.");
        int[][] matrix = createMatrix();
        for (int i = 0; i < matrix.length; i++) {
            if (i == 0 || i == matrix.length - 1) {
                System.out.print("Строка №" + (i + 1) + " : ");
                for (int j : matrix[i]) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }

    static void task5() {
        System.out.println("5. Дана матрица. Вывести на экран все четные строки, то есть с четными номерами.");
        int[][] matrix = createMatrix();
        for (int i = 0; i < matrix.length; i++) {
            if (i % 2 == 1) {
                System.out.print("Строка №" + (i + 1) + " : ");
                for (int j : matrix[i]) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }

    static void task6() {
        System.out.println("6. Дана матрица. Вывести на экран все нечетные столбцы, у которых первый элемент больше последнего.");
        int[][] matrix = createMatrix();
        boolean notFindAnswer = true;
        for (int j = 0; j < matrix[0].length; j++) {
            if (j % 2 == 0 && matrix[0][j] > matrix[matrix.length - 1][j]) {
                notFindAnswer = false;
                System.out.print("Столбец №" + (j + 1) + " : ");
                for (int i = 0; i < matrix.length; i++) {
                    System.out.print(matrix[i][j] + " ");
                }
                System.out.println("");
            }
        }
        if (notFindAnswer) {
            System.out.println("Таких столбцов нет");


        }
    }

    static void task7() {
        System.out.println("7. Дан двухмерный массив 5×5. Найти сумму модулей отрицательных нечетных элементов.");
        int[][] matrix = createMatrix();
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] < 0 && Math.abs(matrix[i][j]) % 2 == 1) {
                    sum += Math.abs(matrix[i][j]);
                }
            }
        }
        System.out.println("Summ = " + sum);
    }


    public static void main(String[] args) {
        task7();


    }
}
