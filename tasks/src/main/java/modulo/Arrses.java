package modulo;

import java.util.ArrayList;
import java.util.Scanner;

public class Arrses {
    public static int getRandomIntBetweenRange(int min, int max) {
        int num = (int) (Math.random() * ((max - min) + 1)) + min;
        return num;
    }

    public static int[] createRandomPositiveArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of elements N: ");
        int n = in.nextInt();
        System.out.println("Array has elements from 0 to M: ");
        System.out.println("Input M: ");
        int m = in.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomIntBetweenRange(0, m);
        }
        printArray(array);
        return array;
    }

    public static void printArray(int[] array) {
        System.out.println("\n");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static int getMaxElement(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    private static int getMinElement(int[] array) {
        int min = array[0];

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

    public static boolean checkSimple(int i){
        if (i<=1)
            return false;
        else if (i <=3)
            return true;
        else if (i%2==0 || i %3 ==0)
            return false;
        int n = 5;
        while (n*n <=i){
            if (i % n ==0 || i % (n+2) == 0)
                return false;
            n=n+6;
        }
        return true;
    }
    private static int[] createRandomIntegerArray() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input amount of elements n: ");
        int n = in.nextInt();
        System.out.println("Array has elements from A to B: ");
        System.out.println("Input A: ");
        int start = in.nextInt();
        System.out.println("Input B: ");
        int finish = in.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = getRandomIntBetweenRange(start, finish);
        }
        printArray(array);
        return array;
    }


    public static void task1() {
        System.out.println("1. В массив A [N] занесены натуральные числа. Найти сумму тех элементов, которые кратны данному К.");
        Scanner in = new Scanner(System.in);
        System.out.print("Input K: ");
        int k = in.nextInt();
        int[] array = createRandomPositiveArray();
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % k == 0) {
                sum += array[i];
            }
        }
        System.out.println("Sum = " + sum);
    }

    public static void task2() {
        System.out.println("2. В целочисленной последовательности есть нулевые элементы. Создать массив из номеров этих элементов.");
        System.out.println("-1;1");
        int[] array = createRandomPositiveArray();
        int counter = 0;
        for (int value : array) {
            if (value == 0) {
                counter++;
            }
        }
        int[] outArr = new int[counter];
        counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                outArr[counter] = i;
                counter++;
            }
        }
        printArray(outArr);
    }

    static void task3() {
        System.out.println("3. Дана последовательность целых чисел а1 а2,..., аn. \nВыяснить, какое число встречается раньше - положительное или отрицательное.");
        int[] array = createRandomIntegerArray();
        for (int value : array) {
            if (value > 0) {
                System.out.println("Positive is ealier");
                break;
            }
            if (value < 0) {
                System.out.println("Negative is ealier");
                break;
            }
        }
    }


    static void task4() {
        System.out.println("4. Дана последовательность действительных чисел а1 а2 ,..., аn . Выяснить, будет ли она возрастающей.");

        int[] array = createRandomIntegerArray();
        boolean isIncrease = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i + 1] < array[i]) {
                isIncrease = false;
            }
        }
        if (isIncrease) {
            System.out.println("It is Increasing!");
        } else {
            System.out.println("It is not Increasing :( ");
        }
    }

    static void task5() {
        System.out.println("5. Дана последовательность натуральных чисел а1 , а2 ,..., аn.\n" +
                "Создать массив из четных чисел этой последовательности.\n" +
                "Если таких чисел нет, то вывести сообщение об этом факте.");

        int[] startArray = createRandomPositiveArray();
        int counter = 0;

        for (int i = 0; i < startArray.length; i++) {
            if (startArray[i] % 2 == 0) {
                counter++;
            }
        }

        int[] printArr = new int[counter];
        if (counter == 0) {
            System.out.println("NO Even Numbers!");
            return;
        }
        counter = 0;
        for (int i = 0; i < startArray.length; i++) {
            if (startArray[i] % 2 == 0) {
                printArr[counter] = startArray[i];
                counter++;
            }
        }
        printArray(printArr);
    }

    static void Task6() {
        System.out.println("6. Дана последовательность чисел а1 ,а2 ,..., ап. \n" +
                "Указать наименьшую длину числовой оси, содержащую все эти числа.");
        int[] array = createRandomIntegerArray();
        int max = getMaxElement(array);
        int min = getMinElement(array);
        System.out.println("Наименьшая длина числовой оси = [" + min + " , " + max + "]");
    }


    static void task7() {
        System.out.println("7. Дана последовательность действительных чисел а1 ,а2 ,..., ап. Заменить все ее члены, большие данного Z, этим числом.\n" +
                "Подсчитать количество замен.");

        Scanner in = new Scanner(System.in);
        System.out.print("Input Z: ");
        int z = in.nextInt();

        int counter = 0;
        int[] array = createRandomIntegerArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] > z) {
                array[i] = z;
                counter++;
            }
        }
        System.out.println("Ответ:");
        printArray(array);
        System.out.println("Количество замен = " + counter);
    }

    static void task8() {
        System.out.println("8. Дан массив действительных чисел, размерность которого N. Подсчитать, сколько в нем отрицательных,\n" +
                "положительных и нулевых элементов.");

        int countPositiv = 0;
        int countNegativ = 0;
        int countZero = 0;
        int[] array = createRandomIntegerArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                countPositiv++;
            }
            if (array[i] < 0) {
                countNegativ++;
            }
            if (array[i] == 0) {
                countZero++;
            }
        }
        System.out.println("Положительных - " + countPositiv);
        System.out.println("Отрицательных - " + countNegativ);
        System.out.println("Нулевых - " + countZero);
    }

    static void task9() {
        System.out.println("9. Даны действительные числа а1 ,а2 ,..., аn . Поменять местами наибольший и наименьший элементы.");

        int[] array = createRandomIntegerArray();

        int maxElement = getMaxElement(array);
        int minElement = getMinElement(array);

        System.out.println("max element = " + maxElement);
        System.out.println("min element = " + minElement);

        for (int i = 0; i < array.length; i++) {
            if (array[i] == maxElement) {
                array[i] = minElement;
            } else {
                if (array[i] == minElement) {
                    array[i] = maxElement;
                }
            }
        }
        printArray(array);
    }

    static void task10() {
        System.out.println("10. Даны целые числа а1 ,а2 ,..., аn . Вывести на печать только те числа, для которых аi > i.");
        int[] array = createRandomIntegerArray();
        System.out.println("аi > i :");
        for (int i = 0; i < array.length; i++) {
            if (array[i] > i) {
                System.out.print(" " + array[i]);
            }
        }
        System.out.println();
    }

    static void task11() {
        System.out.println("11. Даны натуральные числа а1 ,а2 ,..., аn . Указать те из них, у которых остаток от деления на М равен L (0 < L < М-1).");
        int[] array = createRandomPositiveArray();
        Scanner in = new Scanner(System.in);
        System.out.print("Input M: ");
        int m = in.nextInt();
        System.out.println(" Answer is");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % m > 0 && array[i] % m < m - 1) {
                System.out.print(" " + array[i]);
            }
        }
        System.out.println();
    }
    static void task12(){
        System.out.println("12. Задана последовательность N вещественных чисел. Вычислить сумму чисел, порядковые номера которых являются\n" +
                "простыми числами.");
        int[] array = createRandomPositiveArray();
        int sum =0;
        for (int i:array
             ) {
           if(checkSimple(i)){
               sum+=i;
               System.out.println(i+" ");
           }
        }
        System.out.println("Sum of simple nums of arr = "+ sum);
    }

    static void task13(){
        System.out.println("13. Определить количество элементов последовательности натуральных чисел, кратных числу М и заключенных в\n" +
                "промежутке от L до N.");

    }
    public static void main(String[] args) {
        task12();
    }
}


