package calc;

import calc.menu.InputMismatchException;
import calc.menu.MenuImpl;
import calc.model.Operation;
import calc.service.OperationService;

import java.util.Scanner;

public class CalcSimple {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MenuImpl menu = new MenuImpl();
        Operation operation = new Operation();
        OperationService operationService = new OperationService(operation);

        while (true) {
            menu.printMenu();
            try {
                switch (scanner.nextInt()) {
                    case 1:
                        operation = operationService.operationFeeling();
                        operationService.operationValidator(operation);
                        operationService.operationPrintResult(operation);
                        break;
                    case 0:
                        System.exit(0);
                        break;
                    default:
                        throw new InputMismatchException("Wrong input variant! Please, try again!");
                }
            } catch (InputMismatchException e) {
                System.out.println(e.massage);
            }
            finally {
                scanner.close();
            }
        }
    }




}
