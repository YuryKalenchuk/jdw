package calc.service;

import calc.model.Operation;

import java.util.Scanner;

public class OperationService {
    private Operation operation;
    private Scanner scanner = new Scanner(System.in);

    public OperationService(Operation operation) {
        this.operation = operation;
    }

    public Operation operationFeeling() {

        System.out.println("Введите первое число");
        operation.setFirstNum(scanner.next());

        System.out.println("Введите второе число");
        operation.setSecondNum(scanner.next());

        System.out.println("Введите знак операции");
        operation.setCharacter(scanner.next());
        return operation;
    }

    public void operationPrintResult(Operation operation) {
        switch (operation.getCharacter()) {
            case "+":
                System.out.println("Результат " + (Integer.parseInt(operation.getFirstNum()) + Integer.parseInt(operation.getSecondNum())));
                break;
            case "-":
                System.out.println("Результат " + (Integer.parseInt(operation.getFirstNum()) - Integer.parseInt(operation.getSecondNum())));
                break;
            case "*":
                System.out.println("Результат " + (Double.parseDouble(operation.getFirstNum()) * Double.parseDouble(operation.getSecondNum())));
                break;
            case "/":
                System.out.println("Результат " + (Double.parseDouble(operation.getFirstNum()) / Double.parseDouble(operation.getSecondNum())));
                break;
        }
    }

    public void operationValidator(Operation operation) {
        while (!Validator.checkInt(operation.getFirstNum())) {
            System.out.println("Введенные данные не поддерживаются ");
            System.out.println("Введите первое число");
            operation.setFirstNum(scanner.next());
        }
        while (!Validator.checkInt(operation.getSecondNum())) {
            System.out.println("Введенные данные не поддерживаются ");
            System.out.println("Введите второе число");
            operation.setSecondNum(scanner.next());
        }
        while (!Validator.checkChar(operation.getCharacter())) {
            System.out.println("Введенная операция не поддерживается ");
            System.out.println("Введите знак операции");
            operation.setCharacter(scanner.next());
        }
    }
}
