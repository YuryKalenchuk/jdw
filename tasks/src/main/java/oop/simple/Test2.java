package oop.simple;

public class Test2 {

    private int first;
    private int second;

    public Test2(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public Test2() {
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
}