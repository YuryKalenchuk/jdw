package oop.simple;

public class Test1 {
    private int first;
    private int second;

    public void printOut() {
        System.out.println("First = " + first + ", Second = " + second);
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void maxValue() {
        if (first >= second) {
            System.out.println("MAX = " + first);
        } else {
            System.out.println("MAX = " + second);
        }
    }

    public void sumOfDigit() {
        System.out.println("Sum of digits = " + (first + second));
    }
}
