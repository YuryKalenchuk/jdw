package oop.simple.task4;

public class TrainUtils {
    public static Train [] sortTraindestinationName(Train [] trains) {
        Train temp;
        for (int i = 0; i < trains.length; i++) {
            for (int j = trains.length - 1; j > i; j--) {
                int compare = trains[i].getDestinationName().compareTo(trains[j].getDestinationName());
                if (compare > 0) {
                    temp = trains[i];
                    trains[i] = trains[j];
                    trains[j] = temp;
                } else if (compare == 0) {
                    if (trains[i].getStartDate().compareTo(trains[j].getStartDate()) > 0) {
                        temp = trains[i];
                        trains[i] = trains[j];
                        trains[j] = temp;
                    }
                }
            }
        }
        return trains;
    }
}
