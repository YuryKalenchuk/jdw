package oop.simple.task4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

public class Main {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat ft = new SimpleDateFormat("HH:mm");
        Train[] trains = new Train[5];
//        for (int i = 4; i > 0; i--) {
//            Train train = new Train();
//            train.setDestinationName("A" + i);
//            train.setTrainNum((int) (Math.random() * 100));
//            train.setStartDate(ft.parse("04:54"));
//            trains[i] = train;
//        }
//        System.out.println(Arrays.toString(trains));
//        System.out.println(Arrays.toString(TrainUtils.sortTraindestinationName(trains)));


        trains[0] = new Train("Gomel", 456, ft.parse("05:58"));
        trains[1] = new Train("Vilnus", 687, ft.parse("10:48"));
        trains[2] = new Train("Gomel", 931, ft.parse("13:15"));
        trains[3] = new Train("Berlin", 99, ft.parse("18:41"));
        trains[4] = new Train("Gomel", 133, ft.parse("06:40"));
        System.out.println(Arrays.toString(trains));

        System.out.println(Arrays.toString(TrainUtils.sortTraindestinationName(trains)));
    }
}
