package oop.simple.task4;

import java.time.LocalDate;
import java.util.Date;

public class Train {
    private String destinationName;
    private int trainNum;
    private Date startDate;

    Train(String destinationName, int trainNum, Date startDate) {
        this.destinationName = destinationName;
        this.trainNum = trainNum;
        this.startDate = startDate;
    }
    Train(){

    }
    @Override
    public String toString() {
        return "Train{" +
                "destinationName='" + destinationName + '\'' +
                ", trainNum=" + trainNum +
                ", startDate=" + startDate +
                '}';
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public int getTrainNum() {
        return trainNum;
    }

    public void setTrainNum(int trainNum) {
        this.trainNum = trainNum;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

}

