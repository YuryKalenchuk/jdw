package oop.simple.task7;

public class Tringle {
    private double aLength;
    private double bLength;
    private double cLength;

    private double x1;
    private double y1;
    private double x2;
    private double y2;
    private double x3;
    private double y3;
    public Tringle(double x1, double y1, double x2, double y2, double x3, double y3){
        this.aLength=mathLenthOnKoord(x1, y1, x2, y2);
        this.bLength=mathLenthOnKoord(x2, y2, x3, y3);
        this.cLength=mathLenthOnKoord(x1, y1, x3, y3);
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;

    }
    public  void printTringleParametrs() {
        double p = (aLength + bLength + cLength) / 2;
        double s = Math.sqrt(p * (p - aLength) * (p - bLength) * (p - cLength));
        System.out.println("Perimetr of tringle P= " + (aLength + bLength + cLength) + "Area  S= " + s);
    }
    public void printMedianCrosspoin(){
       double xm = (x3+x1)/2;
       double ym = (y3+y1)/2;
       double xCross = (x2+xm)*2/3;
       double yCross = (y2 + ym)*2/3;
        System.out.println("Coord of midians cross is {"+xCross+" : "+yCross+"}");

    }
    private static Double mathLenthOnKoord(double x1, double y1, double x2, double y2) {
        Double d = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        return d;
    }
}
