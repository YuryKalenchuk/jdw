package oop.simple.task6;

public class Task6 {
    public static void main(String[] args) {
        TimeImplementor myTime=new TimeImplementor();
        System.out.println("Defoult value "+myTime.toString());
        myTime.setHours(6);
        System.out.println("Set hour to 6 "+ myTime.toString());
        myTime.setMinutes(41);
        System.out.println("Set min to 41 "+ myTime.toString());
        myTime.setSeconds(13);
        System.out.println("Set sec to 13 "+myTime.toString());
        myTime.setHours(67);
        System.out.println("Set hour to 67 "+ myTime.toString());
        myTime.setMinutes(-1);
        System.out.println("Set min to -41 "+ myTime.toString());
        myTime.setSeconds(131);
        System.out.println("Set sec to 131 "+myTime.toString());
    }
}
