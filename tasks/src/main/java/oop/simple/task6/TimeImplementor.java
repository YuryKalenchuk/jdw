package oop.simple.task6;

import java.util.Iterator;

public class TimeImplementor {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeImplementor() {

    }

    public TimeImplementor(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    @Override
    public String toString() {
        return "Time is {" + hours +
                ":" + minutes +
                ":" + seconds +
                '}';
    }

    static int validateSeconds(int sec) {
        if (sec >= 60 || sec < 0) {
            return 0;
        } else return sec;
    }

    static int validateMinutes(int minutes) {
        if (minutes >= 60 || minutes < 0) {
            return 0;
        } else return minutes;

    }

    static int validateHours(int hours) {
        if (hours >= 24 || hours < 0) {
            return 0;
        } else return hours;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = validateHours(hours);
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = validateMinutes(minutes);
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = validateSeconds(seconds);
    }
}
