package oop.simple.task5;

public class Main {
    public static void main(String[] args) {
        Counter counter = new Counter();
        System.out.println("Defoult value = "+counter.getI());
        Counter counter1 = new Counter(345);
        System.out.println("Castom value = "+counter1.getI() );
        counter1.increnent();
        System.out.println("Incremented value = "+counter1.getI());
        counter.decrement();
        System.out.println("Decremented value of Defoult counter = "+counter.getI() );
    }

}
