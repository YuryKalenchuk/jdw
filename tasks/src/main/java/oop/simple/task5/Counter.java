package oop.simple.task5;

public class Counter {
    private int i;

    public Counter() {
        this.i = 0;//defoult value
    }

    public Counter(int i) {
        this.i = i;
    }

    public void increnent() {
        this.i += 1;
    }

    public void decrement() {
        this.i -= 1;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
