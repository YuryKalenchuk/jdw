package oop.basic.task5;

public class FlowerPaper {
    String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public FlowerPaper(String color) {
        this.color = color;
    }
}
