package oop.basic.task5;

public interface Builder {
    void setFlowers(Flowers flowers);
    void setPaper(FlowerPaper paper);
    void setFlowerFilm(FlowerFilm film);
    void setBand(Band band);
}
