package oop.basic.task5;

public class Bouquet {
    private Flowers flowers;
    private FlowerPaper paper;
    private FlowerFilm film;
    private Band band;

    public Bouquet(Flowers flowers, FlowerPaper paper, FlowerFilm film, Band band) {
        this.flowers = flowers;
        this.paper = paper;
        this.film = film;
        this.band = band;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void setFlowers(Flowers flowers) {
        this.flowers = flowers;
    }

    public FlowerPaper getPaper() {
        return paper;
    }

    public void setPaper(FlowerPaper paper) {
        this.paper = paper;
    }

    public FlowerFilm getFilm() {
        return film;
    }

    public void setFilm(FlowerFilm film) {
        this.film = film;
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }
}
