package oop.basic.task5;

public class BouquetBuilder implements Builder {
    private Flowers flowers;
    private FlowerPaper paper;
    private FlowerFilm film;
    private Band band;

    @Override
    public void setFlowers(Flowers flowers) {
        this.flowers = flowers;
    }

    @Override
    public void setPaper(FlowerPaper paper) {
        this.paper = paper;
    }

    @Override
    public void setFlowerFilm(FlowerFilm film) {
        this.film = film;
    }

    @Override
    public void setBand(Band band) {
        this.band = band;
    }
    public Bouquet getResult(){
        return new Bouquet(flowers,paper,film,band);
    }
}
