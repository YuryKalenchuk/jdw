package oop.basic.task1;

public class task1 {
    public static void main(String[] args) {


        Directory directory = new Directory("Java web development");
        TextFile textFile = new TextFile(directory, "Task1");
        textFile.addText("Hello World");
        textFile.changeName("Test renaming");
        textFile.printContent();
        textFile.addText(" Java is amazing");
        textFile.printContent();
        TextFile fileForDel = new TextFile(directory, "fileForDel");
        directory.printContent();
        System.out.println("now there are 2 files in dir");
        directory.removeFile("fileForDel");
        System.out.println("now there are only one file");
        directory.printContent();
    }
}
