package oop.basic.task2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Payment {
    private BigDecimal overalCost = new BigDecimal(0);
    private List<Unit> list = new ArrayList<>();




    public void addUnit(String name,double price){
        Unit unit = new Unit(name,BigDecimal.valueOf(price));
        list.add(unit);
        overalCost = overalCost.add(unit.getCost());
       // System.out.println("OveralCost is "+overalCost+" now");
    }
    public void printPurchaseList(){
        int i=1;
        System.out.println("Your Purchase list is: ");
        for (Unit unit : list) {
            System.out.println(i+". "+unit.getName()+" "+unit.getCost() );
            i++;
        }
        System.out.println("Yout overal cost is "+overalCost );
    }
    public BigDecimal getOveralCost() {
        return overalCost;
    }

    public void setOveralCost(BigDecimal overalCost) {
        this.overalCost = overalCost;
    }

    public List<Unit> getList() {
        return list;
    }

    public void setList(List<Unit> list) {
        this.list = list;
    }
}

class Unit {
    private String name;
    private BigDecimal cost;

    Unit(String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}