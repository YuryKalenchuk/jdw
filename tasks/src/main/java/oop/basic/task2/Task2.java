package oop.basic.task2;

public class Task2 {
    public static void main(String[] args) {
        Payment p1 = new Payment();
        p1.addUnit("Book",20.3333);
        p1.addUnit("TextBook", 7.2 );
        p1.printPurchaseList();
    }
}
